#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionInit_triggered()
{
    if (b_busy) return;
    b_busy = true;
    std::cout << "Init." << std::endl;

    create_population();
    present_population(true);
    b_busy = false;
}

unsigned int MainWindow::on_actionIterate_triggered()
{
    if (b_busy) return 0;
    b_busy = true;
    std::cout << "Iterate." << std::endl;

    //:::PRE
    if (population_vector.size() < 1) return 0;
    population_ptr current_population = population_vector.at(population_vector.size() - 1);
    population_ptr population(new Artificial::Genetic::Population(0));
    unsigned int limit = population.get()->get_population_limit();

    //:::FILL
    while(population.get()->get_eden().size() < limit) {
        std::vector<eden_ptr> family = current_population.get()->iterate_eden();
        population.get()->insert_family(family);
    }

    //:::STATS
    collection_ptr collection = collection_container.at(collection_container.size() - 1);
    double min = population.get()->get_fitness_min(),
            max = population.get()->get_fitness_max(),
            avg = population.get()->get_fitness_avg();
    collection.get()->insert_statistics(min, max, avg);
    collection_container.push_back(collection);

    //:::END
    population_vector.push_back(population);
    present_population(false);
    b_busy = false;
    return 1;
}

void MainWindow::create_population()
{
    //:::POP
    if (population_vector.size() > 0) population_vector.clear();
    population_ptr population(new Artificial::Genetic::Population());
    population_vector.push_back(population);

    //:::COLL
    if (collection_container.size() > 0) collection_container.clear();
    collection_ptr collection(new Artificial::Statistics::Collection);
    double min = population.get()->get_fitness_min(),
            max = population.get()->get_fitness_max(),
            avg = population.get()->get_fitness_avg();
    collection.get()->insert_statistics(min, max, avg);
    collection_container.push_back(collection);
}

void MainWindow::present_population(bool b_init)
{
    //:::PRE
    population_ptr current_population = population_vector.at(population_vector.size() - 1);
    std::vector<eden_ptr> current_eden = current_population.get()->get_eden();
    unsigned int eden_size = current_eden.size();
    QVector<double> eden_seed(eden_size), eden_fitness(eden_size);

    //:::FILL
    for (unsigned int i = 0; i < eden_size; i++) {
        eden_ptr current_element = current_eden.at(i);
        eden_seed[i] = current_element.get()->get_composite();
        eden_fitness[i] = current_element.get()->get_fitness();
    }

    //:::END
    if (b_init) set_plot(current_population, eden_seed, eden_fitness);
    ui->gfirst->graph(1)->setData(eden_seed, eden_fitness);
    ui->gfirst->replot();

    //:::STATS_RANGE
    collection_ptr collection = collection_container.at(collection_container.size() - 1);
    ui->gsecond->xAxis->setRange(-1,  collection.get()->get_size());
    ui->gsecond->yAxis->setRange(std::min(collection.get()->get_lowest() * 0.9, collection.get()->get_lowest() * 1.1),  collection.get()->get_highest() * 1.1);

    //:::STATS_END
    QVector<double> collection_instance = collection.get()->get_instance();
    ui->gsecond->graph(0)->setData(collection_instance, collection.get()->get_avg());
    ui->gsecond->graph(1)->setData(collection_instance, collection.get()->get_min());
    ui->gsecond->graph(2)->setData(collection_instance, collection.get()->get_max());
    ui->gsecond->replot();
}

void MainWindow::set_plot(population_ptr current_population, QVector<double> q_seed, QVector<double> q_fitness)
{
    //:::POP_X
    double xMin = current_population.get()->get_val_min() * SCALE_PLOT;
    double xMax = current_population.get()->get_val_max() * SCALE_PLOT;
    double xOffset = std::max(std::abs(xMin), std::abs(xMax));
    ui->gfirst->xAxis->setRange(current_population.get()->get_val_min() - xOffset,  current_population.get()->get_val_max() + xOffset);

    //:::POP_Y
    double yMin = current_population.get()->get_fitness_min() * SCALE_PLOT;
    double yMax = current_population.get()->get_fitness_max() * SCALE_PLOT;
    double yOffset = std::max(std::abs(yMin), std::abs(yMax));
    ui->gfirst->yAxis->setRange(current_population.get()->get_fitness_min() - yOffset, current_population.get()->get_fitness_max() + yOffset);

    //:::LINE
    ui->gfirst->addGraph();
    ui->gfirst->graph(0)->setData(q_seed, q_fitness);

    //:::LIFE
    ui->gfirst->addGraph();
    ui->gfirst->graph(1)->setLineStyle(QCPGraph::lsNone);

    //:::SCATT
    QCPScatterStyle scatter_style;
    scatter_style.setSize(10);
    scatter_style.setShape(QCPScatterStyle::ssCircle);
    QPen scatter_pen;
    scatter_pen.setBrush(Qt::blue);
    scatter_style.setPen(scatter_pen);
    scatter_style.setBrush(Qt::red);
    ui->gfirst->graph(1)->setScatterStyle(scatter_style);

    //:::MIN
    ui->gsecond->addGraph();
    //ui->gsecond->graph(0)->setLineStyle(QCPGraph::lsNone);
    scatter_style.setBrush(Qt::yellow);
    ui->gsecond->graph(0)->setScatterStyle(scatter_style);

    //:::MAX
    ui->gsecond->addGraph();
    //ui->gsecond->graph(1)->setLineStyle(QCPGraph::lsNone);
    scatter_style.setBrush(Qt::green);
    ui->gsecond->graph(1)->setScatterStyle(scatter_style);

    //:::AVG
    ui->gsecond->addGraph();
    //ui->gsecond->graph(2)->setLineStyle(QCPGraph::lsNone);
    scatter_style.setBrush(Qt::cyan);
    ui->gsecond->graph(2)->setScatterStyle(scatter_style);
}

void MainWindow::on_initPush_clicked()
{
    on_actionInit_triggered();
}

void MainWindow::on_iteratePush_clicked()
{
    on_actionIterate_triggered();
}

void MainWindow::on_genPush_clicked()
{
    population_ptr current_population = population_vector.at(population_vector.size() - 1);
    std::vector<eden_ptr> current_eden = current_population.get()->get_eden();
    ui->logText->setPlainText("Generating...");
    for (unsigned int i = 0, size = current_eden.size(); i < size; i++) {
        eden_ptr current_life = current_eden.at(i);
        QString current_text = "I: ";
        current_text.append(QString::number(i));
        current_text.append(" Fit: ");
        current_text.append(QString::number(current_life.get()->get_fitness(), 'f', current_life.get()->get_precision()));
        std::vector<std::string> DNAs = current_life.get()->get_DNA();
        std::vector<double> seed = current_life.get()->get_seed();
        for (unsigned int j = 0, life_size = DNAs.size(); j < life_size; j++) {
            current_text.append(" J: ");
            current_text.append(QString::number(j));
            current_text.append(" DNA: ");
            current_text.append(QString::fromStdString(DNAs.at(j)));
            current_text.append(" Seed: ");
            current_text.append(QString::number(seed.at(j), 'f', current_life.get()->get_precision()));
        }
        ui->logText->appendPlainText(current_text);
    }
    ui->logText->appendPlainText("End...");
}

void MainWindow::on_loopPush_clicked()
{
    unsigned int index = 0;

    while (index < 100) {
        index += on_actionIterate_triggered();
    }
}
