#-------------------------------------------------
#
# Project created by QtCreator 2013-10-16T19:22:22
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = ai_ga_01
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    lifeform.cpp \
    core.cpp \
    qcustomplot.cpp \
    population.cpp \
    collection.cpp

HEADERS  += mainwindow.h \
    lifeform.h \
    core.h \
    qcustomplot.h \
    population.h \
    collection.h

FORMS    += mainwindow.ui
CONFIG += c++11

OTHER_FILES +=
