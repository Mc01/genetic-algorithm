#include "core.h"

using namespace Artificial;

signed int Core::Policy::VAL_MIN = -100;
signed int Core::Policy::VAL_MAX = 100;
unsigned int Core::Policy::DNA_NUMBER = 1;
unsigned int Core::Policy::POPULATION_SIZE = 128;
unsigned int Core::Policy::TOURNAMENT_SIZE = 36;
double Core::Policy::MUTATION_RATIO = 0.2;

int Core::Utils::get_interval(int numb) {
    if (numb < 1) return 1;
    else if (numb < 10) return 10;
    else if (numb < 100) return 100;
    else return 1000;
}

int Core::Utils::parse_int(std::string substring)
{
    int result = 0, power = 0;
    for (int i = substring.length() - 1; i > -1; i--) {
        if (substring.at(i) == '1') {
            result += std::pow(2, power);
        }
        power++;
    }
    return result;
}

bool Core::Random::Engine::initialized = false;

double Core::Random::Engine::get_random(double left, double right)
{
    static std::default_random_engine engine {};
    if (!Core::Random::Engine::initialized) {
        engine.seed(std::time(NULL));
        Core::Random::Engine::initialized = true;
    }
    using Distribution = std::uniform_real_distribution<double>;
    static Distribution random {};

    if (left < right) return random(engine, Distribution::param_type{left, right});
    else return random(engine, Distribution::param_type{right, left});
}

