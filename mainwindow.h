#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "population.h"
#include "collection.h"
#include <QMainWindow>
#include <iostream>

typedef std::shared_ptr<Artificial::Genetic::Population> population_ptr;
typedef std::shared_ptr<Artificial::Statistics::Collection> collection_ptr;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
private slots:
    void on_actionInit_triggered();
    unsigned int on_actionIterate_triggered();
    void on_initPush_clicked();
    void on_iteratePush_clicked();
    void on_genPush_clicked();
    void on_loopPush_clicked();
private:
    Ui::MainWindow *ui;
    std::vector<population_ptr> population_vector;
    std::vector<collection_ptr> collection_container;
    bool b_busy = false;
    const double SCALE_PLOT = 0.2;
    void create_population();
    void present_population(bool);
    void set_plot(population_ptr, QVector<double>, QVector<double>);
};

#endif // MAINWINDOW_H
