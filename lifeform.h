#ifndef LIFEFORM_H
#define LIFEFORM_H

#include "core.h"
#include <bitset>
#include <memory>

namespace Artificial
{
    namespace Genetic
    {
        class Lifeform : protected Core::Encode, protected Core::Policy, protected Core::Utils, protected Core::Random::Engine
        {
        public:
            Lifeform();
            Lifeform(std::vector<std::string>);
            //:::GET
            std::vector<std::string> get_DNA() const;
            std::vector<double> get_seed() const;
            double get_composite() const;
            double get_fitness() const;
            double translate_DNA(std::string);
            unsigned int get_precision() const;
        private:
            void absence() {}
            std::vector<std::string> DNAs;
            static double DNA_to_seed(std::string);
            static std::string seed_to_DNA(double);
            static double seed_to_fitness(std::vector<double>);
        };
    }
}

#endif // LIFEFORM_H
