#ifndef CORE_H
#define CORE_H

#include <string>
#include <random>
#include <chrono>

namespace Artificial {
    namespace Core
    {
        class Encode
        {
        protected:
            static const char ENCODE_PLUS = '+';
            static const char ENCODE_MINUS = '-';
            static const char ENCODE_DOT = '.';
            static const char ENCODE_ONE = '1';
            static const char ENCODE_ZERO = '0';
        private:
            virtual void absence() = 0;
        };

        class Policy
        {
        protected:
            //:::CONF
            static signed int VAL_MIN;
            static signed int VAL_MAX;
            static unsigned int DNA_NUMBER;
            static unsigned int POPULATION_SIZE;
            static unsigned int TOURNAMENT_SIZE;
            static double MUTATION_RATIO;
            //:::CONST
            static const signed int VAL_DEF = 0;
            static const unsigned int BITSET_SIZE = 10;
            static const unsigned int RANGE_PRECISION = 3;
        private:
            virtual void absence() = 0;
        };

        class Utils
        {
        protected:
            static int get_interval(int);
            static int parse_int(std::string);
        private:
            virtual void absence() = 0;
        };

        namespace Random
        {
            class Engine
            {
            protected:
                static double get_random(double, double);
            private:
                virtual void absence() = 0;
                static bool initialized;
            };
        }
    }
}

#endif // CORE_H
