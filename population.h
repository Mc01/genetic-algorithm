#ifndef POPULATION_H
#define POPULATION_H

#include "lifeform.h"

typedef std::shared_ptr<Artificial::Genetic::Lifeform> eden_ptr;

namespace Artificial {
    namespace Genetic {
        class Population : protected Core::Encode, protected Core::Policy, protected Core::Random::Engine
        {
        public:
            Population();
            Population(int);
            virtual ~Population();
            //:::GET
            std::vector<eden_ptr> get_eden();
            unsigned int get_population_limit() const;
            signed int get_val_min() const;
            signed int get_val_max() const;
            double get_fitness_avg();
            double get_fitness_min();
            double get_fitness_max();
            //:::OUT
            void insert_family(std::vector<eden_ptr>);
            std::vector<eden_ptr> iterate_eden();
        private:
            void absence() {}
            std::vector<eden_ptr> eden_vector;
            const unsigned int STATISTICS_MIN = 0;
            const unsigned int STATISTICS_AVG = 1;
            const unsigned int STATISTICS_MAX = 2;
            const unsigned int STATISTICS_TOTAL = 3;
            //:::TOUR
            std::vector<eden_ptr> collect_participants();
            std::vector<eden_ptr> find_winners(std::vector<eden_ptr>);
            std::vector<eden_ptr> pop_out(std::vector<eden_ptr>, unsigned int);
            //:::CROSS
            std::vector<eden_ptr> cross_over(std::vector<eden_ptr>);
            eden_ptr cross_mutate(eden_ptr, eden_ptr, unsigned int, unsigned int);
            std::string mutate(std::string);
            unsigned int extract_bits(signed int, signed int);
            //:::STATS
            double gather_statistics(std::vector<eden_ptr>, unsigned int, double);
        };
    }
}

#endif // POPULATION_H
