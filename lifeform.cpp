#include "lifeform.h"
#include <iostream>

using namespace Artificial;

Genetic::Lifeform::Lifeform()
{
    for (unsigned int i = 0; i < DNA_NUMBER; i++) {
        this->DNAs.push_back(seed_to_DNA(get_random(VAL_MIN, VAL_MAX)));
    }
}

Genetic::Lifeform::Lifeform(std::vector<std::string> DNAs)
{
    this->DNAs = DNAs;
}

std::vector<std::string> Genetic::Lifeform::get_DNA() const
{
    return this->DNAs;
}

std::vector<double> Genetic::Lifeform::get_seed() const
{
    std::vector<double> seed;
    for (unsigned int i = 0, size = DNAs.size(); i < size; i++) {
        seed.push_back(DNA_to_seed(this->DNAs.at(i)));
    }
    return seed;
}

double Genetic::Lifeform::get_composite() const
{
    double composite = 0;
    for (unsigned int i = 0, size = DNAs.size(); i < size; i++) {
        composite += DNA_to_seed(DNAs.at(i));
    }
    return composite;
}

double Genetic::Lifeform::get_fitness() const
{
    return seed_to_fitness(this->get_seed());
}

double Genetic::Lifeform::translate_DNA(std::string DNA)
{
    return DNA_to_seed(DNA);
}

unsigned int Genetic::Lifeform::get_precision() const
{
    return RANGE_PRECISION;
}

double Genetic::Lifeform::DNA_to_seed(std::string DNA)
{
    //locate dot
    std::size_t dot_found = DNA.find(ENCODE_DOT);
    if (dot_found==std::string::npos) {
        std::cout << "VAL DEF! (no_dot) " << DNA << std::endl;
        return VAL_DEF;
    }

    //back
    std::string back_substring;
    for (unsigned int i = dot_found + 1, end = DNA.length(); i < end; i++) {
        back_substring += DNA.at(i);
    }
    double seed = parse_int(back_substring);
    seed /= get_interval(seed);

    //front
    std::string front_substring;
    for (unsigned int i = 1; i < dot_found; i++) {
        front_substring += DNA.at(i);
    }
    seed += parse_int(front_substring);

    //sign
    char sign = DNA.at(0);
    if (sign == ENCODE_MINUS) {
        return -1 * seed;
    }
    else if (sign == ENCODE_PLUS){
        return seed;
    }
    else {
        std::cout << "VAL DEF! (no_sign) " << DNA << " : " << seed << std::endl;
        return VAL_DEF;
    }
}

std::string Genetic::Lifeform::seed_to_DNA(double seed) //+- 0-9 . 00-99
{
    std::string child_DNA = "";

    //+ or -
    if (seed < 0) {
        child_DNA += ENCODE_MINUS;
    }
    else {
        child_DNA += ENCODE_PLUS;
    }

    //trim set
    int trim_seed = std::abs(seed);
    std::bitset< BITSET_SIZE >  trim_set (trim_seed);
    child_DNA += trim_set.to_string();

    //dot
    child_DNA += ENCODE_DOT;

    //crunch seed
    int crunch_seed = (int) seed;
    seed = seed - crunch_seed;
    seed *= std::pow(10, RANGE_PRECISION);
    crunch_seed =  std::abs(std::roundf(seed));

    //crunch set
    std::bitset< BITSET_SIZE > crunch_set (crunch_seed);
    child_DNA += crunch_set.to_string();

    return child_DNA;
}

double Genetic::Lifeform::seed_to_fitness(std::vector<double> seed)
{
    unsigned int size = seed.size();
    if (size == 0) return 0;
    else if (size == 1) {
        return std::pow(seed.at(0), 3) - seed.at(0) * 2;
        //return std::pow(seed.at(0), 2) + seed.at(0) * 2;
    }
    else if (size == 2){
        return 1.0316285 + 4 * std::pow(seed.at(0), 2) - 2.1 * std::pow(seed.at(0), 4)
                + std::pow(seed.at(0), 6) / 3 + seed.at(0) * seed.at(1)
                - 4 * std::pow(seed.at(1), 2) + 4 * std::pow(seed.at(1), 4);
    }
    else {
        return std::pow(seed.at(0), 3) - seed.at(1) * 2 + seed.at(2);
    }
}
