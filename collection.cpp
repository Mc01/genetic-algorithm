#include "collection.h"

void Artificial::Statistics::Collection::insert_statistics(double min, double max, double avg)
{
    instance_vector.push_back(instance_vector.size());
    min_vector.push_back(min);
    max_vector.push_back(max);
    avg_vector.push_back(avg);
}

QVector<double> Artificial::Statistics::Collection::get_instance()
{
    return instance_vector;
}

QVector<double> Artificial::Statistics::Collection::get_min()
{
    return min_vector;
}

QVector<double> Artificial::Statistics::Collection::get_max()
{
    return max_vector;
}

QVector<double> Artificial::Statistics::Collection::get_avg()
{
    return avg_vector;
}

unsigned int Artificial::Statistics::Collection::get_size()
{
    return instance_vector.size();
}

double Artificial::Statistics::Collection::get_lowest()
{
    return get_data(min_vector, true);
}

double Artificial::Statistics::Collection::get_highest()
{
    return get_data(max_vector, false);
}

double Artificial::Statistics::Collection::get_data(QVector<double> source, bool b_min)
{
    double statistics = source.at(0);
    for (unsigned int i = 1, size = source.size(); i < size; i++) {
        if (b_min) statistics = std::min(statistics, source.at(i));
        else statistics = std::max(statistics, source.at(i));
    }
    return statistics;
}
