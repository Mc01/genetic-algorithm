#include "population.h"

using namespace Artificial;

Genetic::Population::Population()
{
    for (unsigned int i = 0; i < POPULATION_SIZE; i++) {
        eden_ptr eden(new Genetic::Lifeform());
        eden_vector.push_back(eden);
    }
}

Genetic::Population::Population(int size)
{
    for (int i = 0; i < size; i++) {
        eden_ptr eden(new Genetic::Lifeform());
        eden_vector.push_back(eden);
    }
}

Genetic::Population::~Population()
{
    eden_vector.clear();
}

std::vector<eden_ptr> Genetic::Population::get_eden()
{
    return eden_vector;
}

std::vector<eden_ptr> Genetic::Population::iterate_eden()
{
    std::vector<eden_ptr> generation;
    generation = collect_participants();
    generation = cross_over(generation);
    return generation;
}

unsigned int Genetic::Population::get_population_limit() const
{
    return POPULATION_SIZE;
}

signed int Genetic::Population::get_val_min() const
{
    return VAL_MIN;
}

signed int Genetic::Population::get_val_max() const
{
    return VAL_MAX;
}

double Genetic::Population::get_fitness_avg()
{
    return gather_statistics(eden_vector, STATISTICS_AVG, 0);
}

double Genetic::Population::get_fitness_min()
{
    return gather_statistics(eden_vector, STATISTICS_MIN, 0);
}

double Genetic::Population::get_fitness_max()
{
    return gather_statistics(eden_vector, STATISTICS_MAX, 0);
}

void Genetic::Population::insert_family(std::vector<eden_ptr> family_vector)
{
    for (unsigned int i = 0, size = family_vector.size(); i < size; i++) {
        if (eden_vector.size() < POPULATION_SIZE) {
            eden_vector.push_back(family_vector.at(i));
        }
    }
}

std::vector<eden_ptr> Genetic::Population::collect_participants()
{
    std::vector<eden_ptr> safe_eden(eden_vector);
    std::vector<eden_ptr> participants;

    for (unsigned int i = 0; i < TOURNAMENT_SIZE; i++) {
        unsigned int random = std::rand() % safe_eden.size();
        participants.push_back(safe_eden.at(random));
        eden_ptr participant = participants.at(i);
        pop_out(safe_eden, random);
    }
    return find_winners(participants);
}

std::vector<eden_ptr> Genetic::Population::find_winners(std::vector<eden_ptr> participants)
{
    std::vector<eden_ptr> winners;
    double current_fitness, total_fitness, offset_fitness, random;
    while (winners.size() < 2) {
        current_fitness = 0;
        offset_fitness = std::abs(gather_statistics(participants, STATISTICS_MIN, -1));
        total_fitness = gather_statistics(participants, STATISTICS_TOTAL,offset_fitness);
        random = get_random(total_fitness, 0);

        for (unsigned int i = 0, size = participants.size(); i < size; i++) {
            eden_ptr current_life = participants.at(i);
            current_fitness += current_life.get()->get_fitness() + offset_fitness;
            if (current_fitness > random) {
                winners.push_back(current_life);
                participants = pop_out(participants, i);
                break;
            }
        }
    }
    return winners;
}

double Genetic::Population::gather_statistics(std::vector<eden_ptr> source, unsigned int mode, double offset)
{
    eden_ptr current_life = source.at(0);
    double statistics = current_life.get()->get_fitness() + offset;
    for (unsigned int i = 1, size = source.size(); i < size; i++) {
        current_life = source.at(i);
        if (mode == STATISTICS_MIN) statistics = std::min(statistics, current_life.get()->get_fitness() + offset);
        else if (mode == STATISTICS_MAX) statistics = std::max(statistics, current_life.get()->get_fitness() + offset);
        else statistics += current_life.get()->get_fitness() + offset;
    }
    if (mode == STATISTICS_AVG) statistics = statistics / source.size();
    return statistics;
}

std::vector<eden_ptr> Genetic::Population::pop_out(std::vector<eden_ptr> eden, unsigned int index)
{
    std::vector<eden_ptr> allocator;
    for (unsigned int i = eden.size() - 1; i > index; i--) {
        allocator.push_back(eden.at(i));
        eden.pop_back();
    }
    eden.pop_back(); //Release winner
    for (int i = (int) (allocator.size() - 1); i > -1; i--) {
        eden.push_back(allocator.at(i));
        allocator.pop_back();
    }
    return eden;
}

std::vector<eden_ptr> Genetic::Population::cross_over(std::vector<eden_ptr> parent_vector)
{
    //:::PRE
    eden_ptr parent_a = parent_vector.at(0), parent_b = parent_vector.at(1), first, second;
    unsigned int number = parent_a.get()->get_DNA().size();
    bool b_issue = true;

    //:::SEARCH
    while (b_issue) {
        //:::INIT
        b_issue = false;
        unsigned int random = std::rand() % (extract_bits(VAL_MIN, VAL_MAX) + BITSET_SIZE);
        first = cross_mutate(parent_a, parent_b, random, number);
        second = cross_mutate(parent_b, parent_a, random, number);

        //:::CHECK
        std::vector<std::string> first_DNA = first.get()->get_DNA();
        std::vector<std::string> second_DNA = second.get()->get_DNA();
        for (unsigned int i = 0, size = first_DNA.size(); i < size; i++) {
            double seed_a = parent_a.get()->translate_DNA(first_DNA.at(i));
            double seed_b = parent_b.get()->translate_DNA(second_DNA.at(i));
            if (VAL_MIN > seed_a || VAL_MIN > seed_b || seed_a > VAL_MAX || seed_b > VAL_MAX) {
                b_issue = true;
            }
        }
    }

    //:::FILL
    parent_vector.push_back(first);
    parent_vector.push_back(second);

    return parent_vector;
}

eden_ptr Genetic::Population::cross_mutate(eden_ptr parent_a, eden_ptr parent_b, unsigned int random, unsigned int number)
{    
    std::vector<std::string> DNAs;
    std::string parentDNA;
    double m_random = get_random(0, 1.0);

    unsigned int pre = BITSET_SIZE - extract_bits(VAL_MIN, VAL_MAX) + 1;
    unsigned int dot = BITSET_SIZE + 1;
    random += pre; //UNUSED BITS, SIGN
    if (random >= dot) random++; //DOT SHIFT

    for (unsigned int i = 0; i < number; i++) {
            //:::CROSS
            parentDNA = parent_a.get()->get_DNA().at(i);
            std::string DNA = parentDNA.substr(0, random);
            parentDNA = parent_b.get()->get_DNA().at(i);
            DNA += parentDNA.substr(random, parentDNA.size());

            //:::MUTATE
            if (m_random < MUTATION_RATIO) DNA = mutate(DNA);
            DNAs.push_back(DNA);
    }

    eden_ptr child(new Genetic::Lifeform(DNAs));
    return child;
}

std::string Genetic::Population::mutate(std::string DNA)
{
    signed int random = rand() % DNA.size();
    char key = DNA.at(random);
    switch (key) {
    case ENCODE_ONE:
        DNA.at(random) = ENCODE_ZERO;
        break;
    case ENCODE_ZERO:
        DNA.at(random) = ENCODE_ONE;
        break;
    case ENCODE_MINUS:
        DNA.at(random) = ENCODE_PLUS;
        break;
    case ENCODE_PLUS:
        DNA.at(random) = ENCODE_MINUS;
    }
    return DNA;
}

unsigned int Genetic::Population::extract_bits(signed int min, signed int max)
{
    int bigger = std::max(std::abs(min), std::abs(max));
    for (unsigned int i = BITSET_SIZE; i > 0; i--) {
        if (bigger - std::pow(2, i) > 0) {
            return i;
        }
    }

    return 0;
}
