#ifndef COLLECTION_H
#define COLLECTION_H

#include <QVector>

namespace Artificial {
    namespace Statistics
    {
        class Collection
        {
        public:
            void insert_statistics(double, double, double);
            QVector<double> get_instance();
            QVector<double> get_min();
            QVector<double> get_max();
            QVector<double> get_avg();
            unsigned int get_size();
            double get_lowest();
            double get_highest();
        private:
            QVector<double> instance_vector;
            QVector<double> min_vector;
            QVector<double> max_vector;
            QVector<double> avg_vector;
            double get_data(QVector<double>, bool);
        };
    }
}

#endif // COLLECTION_H
